import { ProductSpec } from './product-spec';

export const PRODUCTS: ProductSpec[] = [
    {
        id:1,
        title:'Product 1',
        price:'$ 487',
        category:'...',
        description:'...',
        image:'...',
        rating: {"rate":3.9,"count":120}
    },

    {
        id:30,
        title:'Product 2',
        price:'$ 3000',
        category:'...',
        description:'...',
        image:'...',
        rating: {"rate":4.1,"count":"259"}
    }
];