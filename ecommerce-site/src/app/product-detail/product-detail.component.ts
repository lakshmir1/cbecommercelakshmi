import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ProductsService } from '../products.service';
import { ProductSpec } from '../product-spec';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  product: ProductSpec | undefined;

  constructor( private location: Location, private productsService: ProductsService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getProductById();
  }

  getProductById(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.productsService.getProductById(id)
    .subscribe(product => this.product = product)
  }

  goBack(): void {
    this.location.back();
  }

}
