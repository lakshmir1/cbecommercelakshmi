import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductsService } from '../products.service';
import { ProductSpec } from '../product-spec';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  adminForm: FormGroup;
  products: ProductSpec[] = [];
  constructor(private formBuilder: FormBuilder, private router: Router, private productsService: ProductsService) { }

  ngOnInit()  {
    this.initializeForm();
    this.getProducts();
  }

  initializeForm(): void {
    this.adminForm = this.formBuilder.group({

    })
  }

  getProducts(): void {
    //this.products = this.productsService.getProducts();
    this.productsService.getProductHttp()
    .subscribe(products => this.products = products)
  }

  logout(): void {
    //console.log(this.loginForm);
      this.router.navigateByUrl('/login');
  }
}
