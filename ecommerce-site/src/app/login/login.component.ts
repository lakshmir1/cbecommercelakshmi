import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm(): void {
    this.loginForm = this.formBuilder.group({
      useremail: ["me@example.com", [
        Validators.required,
        Validators.email,
      ]],
      pswd: ['', [
        Validators.required,
        //Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])(a-zA-Z0-9]+)$')
      ]]
    })
  }

  get useremail() {
    return this.loginForm.get('useremail');
  }
  get pswd() {
    return this.loginForm.get('pswd');
  }
  onSubmit(): void {
      this.router.navigateByUrl('/admin');
  }

}
